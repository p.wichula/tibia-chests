const container = document.getElementById("chestsContainer");
const itemsArr = [
    { name: "100TC", dropChance: 0.05 , path: 'assets/Tibia_Coins.gif', textColor: 'gold'},
    { name: "Song", dropChance: 0.25, path: 'assets/note.gif', textColor: '#1167af'},
    { name: "25TC", dropChance: 0.20, path: 'assets/Tibia_Coins.gif', textColor: 'green'},
    { name: "Empty!", dropChance: 0.50, path: 'assets/trash.gif', textColor: 'red'},
];

function drawObj() {
    let chanceSum = 0;
    itemsArr.forEach(obj => {
        chanceSum += obj.dropChance;
    });

    const randomNumber = Math.random() * chanceSum;

    let lootedItem = null;
    let actualChance = 0;
    for (let i = 0; i < itemsArr.length; i++) {
        actualChance += itemsArr[i].dropChance;
        if (randomNumber < actualChance) {
            lootedItem = itemsArr[i];
            break;
        }
    }
    return lootedItem;
}

function onStart() {
    for (let i = 0; i < 5; i++) {
        const chestContainer = document.createElement("div");
        const img = document.createElement("img");
        const lootedItem = drawObj();
        const itemName = document.createElement("span");

        chestContainer.classList.add('chest-container');
        itemName.classList.add("item-name");

        img.src = 'assets/Exaltation_Chest.gif';
        img.alt = "chest...";

        chestContainer.appendChild(img);
        chestContainer.appendChild(itemName);

        function handleClick() {
            const lootedItemCopy = {...lootedItem};
            img.src = 'assets/Divine_Grenade_Effect.gif';
            setTimeout(() => {
                img.src = lootedItemCopy.path;
                itemName.textContent = lootedItemCopy.name;
                itemName.style.display = 'inline-block';
                itemName.style.color = lootedItemCopy.textColor;
            }, 1000);

            img.removeEventListener("click", handleClick);
        }

        img.addEventListener("click", handleClick);

        container.appendChild(chestContainer);
    }
}

function reload() {
    location.reload();
}
